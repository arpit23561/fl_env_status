FROM phirov/flask-restful

WORKDIR /usr/src/app

COPY requirements.txt ./1
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./inst-ownership.py" ]