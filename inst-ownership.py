from flask import Flask
from flask_restful import Resource, Api, reqparse
from tinydb import TinyDB, Query

db = TinyDB('db.json')
app = Flask(__name__)
api = Api(app)
Count=Query()
env_status=db.all()

class env(Resource):
    def get(self, count):
            return db.search(Count.Env_Count == count), 200


    def post(self, count):
        parser = reqparse.RequestParser()
        parser.add_argument("Int_Owner")
        parser.add_argument("Dev_Owner")
        parser.add_argument("Purpose")
        args = parser.parse_args()
        for env1 in env_status:
            if(count == int(env1["Env_Count"])):
                return "env with count {} already exists".format(count), 400
        env1 = {
            "Env_Count": count,                        
            "Int_Owner": args["Int_Owner"],
            "Dev_Owner": args["Dev_Owner"],
            "Purpose": args["Purpose"]
        }
        db.insert(env1)
        return env1, 201


    def put(self, count):
        parser = reqparse.RequestParser()
        parser.add_argument("Int_Owner")
        parser.add_argument("Dev_Owner")
        parser.add_argument("Purpose")
        args = parser.parse_args()
        env1 = {
            "Env_Count": count,                        
            "Int_Owner": args["Int_Owner"],
            "Dev_Owner": args["Dev_Owner"],
            "Purpose": args["Purpose"]
        }
        db.update(env1, Count.Env_Count==count)
        return env1, 201


    def delete(self, count):
        db.remove(Count.Env_Count==count)
        return "{} is deleted.".format(count), 200


class all(Resource):
    def get(self):
        return db.all()

api.add_resource(env, "/count/<int:count>")
api.add_resource(all, "/count/")
app.run(debug=True)
